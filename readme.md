# Blackfire enabled PHP development environment

## Setup

### Set your blackfire tokens

Copy the `docker/.env.dev.example` file to `docker/.env` and modify with your tokens.

### Optionally change php ini settings

Copy one of the `docker/Config/php_dev.ini` files to `docker/Config/php.ini`

## Running

```
cd docker
docker-compose up
```

Your site document root is src/public and will be available at http://localhost:8080

## Running a blackfire trace

### The easy way with a GUI

The easiest way to do it is to use the Chrome extension.  

Load the page you want to profile and press the blackfire button.

This is quite limited, you'll want to use the CLI most of the time.

### The easy way with CLI

#### Install the blackfire agent

You only want the agent on your machine, not the probe.

To install the blackfire-agent on your machine see the [blackfire site](https://blackfire.io/docs/up-and-running/installation)

You can also run the Blackfire agent without installing it by using Docker (see [the Blackfire manual](https://blackfire.io/docs/integrations/docker#running-the-agent))

#### Use the agent to profile a curl query

The general format of the command is `blackfire curl <query>`

You can easily get the full curl command for a request by opening up your network traffic inspector in your browser.
Right click the query you want to profile and then "copy as curl" to get the Curl command you can run on CLI to 
generate that query.

Now you can just copy and paste it into your CLI, just adding "blackfire" in front of it.

For example:

```
blackfire curl 'http://localhost:8080/' -H 'DNT: 1' -H 'Accept-Encoding: gzip, deflate, sdch, br' -H 'Accept-Language: en-US,en;q=0.8' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Connection: keep-alive' --compressed
 2076  blackfire curl 'http://localhost:8080/' -H 'DNT: 1' -H 'Accept-Encoding: gzip, deflate, sdch, br' -H 'Accept-Language: en-US,en;q=0.8' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Connection: keep-alive' --compressed

```

That looks complicated, but it's literally copy and paste from my Chrome network inspector.

## Seeing your profiling results

Login to the service and see the dashboard.
